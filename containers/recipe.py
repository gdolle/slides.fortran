#!/usr/bin/env python
import hpccm
import hpccm.primitives
import hpccm.building_blocks
import hpccm.primitives

#hpccm.config.set_container_format('docker')

Stage0 += baseimage(image="ubuntu:19.04")

# Packages
Stage0 += gnu()
Stage0 += python( python2=False )
Stage0 += packages(ospackages=["wget","unzip","julia","texlive","texlive-extra-utils","pandoc","jupyter"])
#Stage0 += ofed()
#Stage0 += openblas()
#Stage0 += openmpi( cuda=False )

Stage0 += pip( packages= ["vdom",
                          "notebook",
                          "rise",
                          "jupyter-contrib-nbextensions",
                          "jupyter_nbextensions_configurator"],
               pip="pip3" )

Stage0 += shell( commands= [
    # FORTRAN
    "wget https://github.com/ZedThree/jupyter-fortran-kernel/archive/master.zip",
    "unzip master.zip && rm -rf master.zip",
    "mv jupyter-fortran-kernel-master jupyter-fortran-kernel",
    "cd jupyter-fortran-kernel",
    "pip3 install .",
    "jupyter-kernelspec install fortran_spec/",
    # JUPYTER EXT
    "jupyter nbextensions_configurator enable --system",
    "jupyter contrib-nbextension install",
    "jupyter nbextension enable --system equation-numbering/main",
    "jupyter nbextension enable --system splitcell/splitcell",
    "jupyter nbextension enable --system spellchecker/main",
    "jupyter nbextension enable --system exercice2/main",
    "jupyter nbextension enable --system datestamper/main",
    "jupyter nbextension enable --system codefolding/main",
    "jupyter nbextension enable --system nbTranslate/main",
    "jupyter nbextension enable --system printview/main",
 ])

# Binder specific user.
Stage0 += raw( docker='ARG NB_USER' )
Stage0 += raw( docker='ARG NB_UID' )
Stage0 += environment(variables={"USER":"${NB_USER}"})
Stage0 += environment(variables={"HOME":"/home/${NB_USER}"})
Stage0 += shell(commands=["adduser --disabled-password --gecos 'Default user' --uid ${NB_UID} ${NB_USER}"] )
Stage0 += copy( src="slides", dest="${HOME}" )
Stage0 += shell(commands=["chown -R ${NB_UID} ${HOME}"] )
Stage0 += workdir(directory="${HOME}")
Stage0 += user(user="${USER}")

# Expose the server
Stage0 += raw( docker='EXPOSE 8888' )

# Autostart
#Stage0 += runscript( commands=['jupyter notebook --ip 0.0.0.0'] )
