## INHERITE!

DECKTAPE_VERSION=2.9.3

CONTNAME=slides-fortran
CURDIR=`pwd`
NPMBIN=`npm bin`

HELP="Usage: make [option]\n\
\n\
options: \n\
\t - install\t install required software (gem).\n\
\t - html\t generate html slides.\n\
\t - pdf\t generate pdf slides.\n\
\n"

#https://github.com/astefanutti/decktape

all: html

html:
	jupyter-nbconvert --to slides slides/slides.ipynb
	
pdf: pdf-decktape

pdf-decktape: html
	${NPMBIN}/decktape --pause 2000 --chrome-path /opt/google/chrome/chrome automatic file:///${CURDIR}/slides/slides.slides.html slides/slides.pdf #--no-sandbox

# CONTAINERS
recipe: recipe-docker recipe-singularity
container: container-docker container-singularity

recipe-docker:
	@hpccm --recipe containers/recipe.py --format docker > containers/rise.docker

recipe-singularity:
	@hpccm --recipe containers/recipe.py --format singularity > containers/rise.singularity

container-docker:
	docker build --build-arg "NB_USER=slides" --build-arg "NB_UID=1000" -t ${CONTNAME} -f containers/rise.docker .

container-singularity:
	sudo singularity build containers/rise.sif containers/rise.singularity

run-docker:
	docker run -p 8888:8888 --rm ${CONTNAME} jupyter notebook --ip 0.0.0.0 --no-browser

install:
	npm install --save

clean:
	@rm -rf node_modules
	@rm -rf package-lock.json

help:
	@printf ${HELP}
